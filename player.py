import random
import os
import numpy as np

class Player():
	"""
	Generic Player Class

	Attributes:
		Dice: 		Integer
		Hand: 		List of integers
		Wins: 		Integer
		Type: 		String, used to determine if it's human player or an AI
		Name: 		String, provides clarity for human players.
		Position:	Integer, helps locate self in game objects passed to the player
	
	Methods:
		New_Hand, draws a new set of dice for the next round
		Bid_Invalid,

	"""

	def __init__(self, n, name, kind, pos):
		self.Dice = n
		self.Type = kind
		self.Name = str(name)
		self.Position = pos

		self.Hand = []
		self.Wins = 0

	def New_Hand(self):
		#draws a new hand for the round
		self.Hand = [random.randint(1, 6) for k in range(self.Dice)]


	def Bid_Invalid(self,bid,new_bid):
		if (type(bid[0]) is not int) or (type(bid[1]) is not int):
			#determines if the bid is in the correct format
			print "wrong type"
			return True
	
		elif bid[1] not in [2,3,4,5,6]:
			#if they chose numbers that aren't valid
			print "not valid dice numbers"
			return True
	
		elif bid[0] > new_bid[0]:
			#amount decreased
			print "you can't lower the amount"
			return True 
	
		elif (bid[0] == new_bid[0]) and (new_bid[1] <= bid[1]):
			#amount stayed the same but the number didn't increase
			print "Not enough"
			return True 
	
		else:
			#the previous conditions should be sufficient
			return False

class Human(Player):

	def player_choice(self,bid,dice,history,opp_dice):
		print "your hand is", self.Hand, " there are " + str(dice) + " dice."
		print "the bid is " + str(bid)
		choice = str(raw_input())

		while (choice != "lie") and (choice != "true"):
			print "please choose, true or lie"
			choice = str(raw_input())

		if choice == "lie":
			#os.system('cls')
			return False, bid


		else:
			#take user input to make create a bid
			print "what is your bid? Amount and then Number"
			amount = int(raw_input())
			number = int(raw_input())
		 	new_bid = [amount,number]
		

			while self.Bid_Invalid(bid,new_bid):
				print "your bid was invalid"
				print "what is your bid? Amount and then Number"
				#if the bid is invalid keep going until it is valid
			 	amount = int(raw_input())
			 	number = int(raw_input())
			 	new_bid = [amount,number]

	 		#os.system('cls')	
			return True, new_bid

class Test_AI(Player):

	def player_choice(self, bid, dice, history, opp_dice):

		hand_count = self.Hand.count(bid[1]) + self.Hand.count(1)

		most_of = [	self.Hand.count(1) + self.Hand.count(2),
		 		   	self.Hand.count(1) + self.Hand.count(3), 
		 			self.Hand.count(1) + self.Hand.count(4),
		 			self.Hand.count(1) + self.Hand.count(5),
		 			self.Hand.count(1) + self.Hand.count(6)]

		most_index = dict((key,value) for (key,value) in zip(most_of,[2,3,4,5,6]) )

		most = most_index[max(most_of)]
		amount = bid[0]
		number = bid[1]


		if amount < dice/3:
			return True, [dice/3,6]

		elif amount == dice/3:
			#print "bid", amount, number
			if number < 6:
				#print "shortcut",amount,number
				return True, [amount,6]
			else:
				#print amount+1 , most
				return False, bid

		else:
			return False, bid


class Player_AI(Player):
	#A really dumb AI

	def player_choice(self, bid, dice, history, opp_dice):

		"""
		Write a method that either makes a new bid or calls the previous bid a lie.

		
		
							Information provided:		

		Hand: 		self.Hand, 		a list of the values of the dice in your hand
		dice: 		dice,			an integer that contains the total number of dice left in the game
		bid: 		bid,			a list consisting of [amount, value]
		history:	history,		a list of lists that contains all previous bids [bid, ..., bid]	
		other:		opp_dice,		a list of integers that contains the number of remaining dice in your opponents' hands
					self.Position	an integer that correlates to which seat the player is in. (helpful for using history and opp_dice) 
		
							Outputs:

		Liar!:		return False, bid  		the first part is a boolean, the second is the previous bid
		My bid:		return True, new_bid	the first part is a boolean, the second is list in the format [amount, number]


							Notes:

		There is an assumption that you will only create valid bids, if you are found to be submitting invald bids you will be disqualified.
		It is especially important if you're doing a decision tree that you have the proper returns for each branch of the tree

		"""
		if bid[0] > ((dice)/3) - np.count_nonzero(self.Hand[self.Hand==bid[1]]):
			return False, bid
		if bid[0] > np.count_nonzero(self.Hand[self.Hand==bid[1]]) + (1./3)*(dice*5./6)+1:
			#print "I think you're a liarrrrr"
			return False, bid


		else:
			amount = bid[0]
			if amount < 1:
				amount = 1
			number = bid[1] + 1

			if number > 6:
				amount += 1
				number = 2

			new_bid = [amount, number]		

			return True, new_bid
