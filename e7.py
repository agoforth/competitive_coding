def isPrime(n):
    if n ==2: return True
    if n % 2 == 0 or n < 2: return False
    for i in range(3, int(n**0.5)+1, 2):
	if n % i == 0: return False
    return True

def findNthPrime(n):
    prime = 2
    count = 1
    itr = 3
    while count < n:
	if isPrime(itr):
	    prime = itr
	    count += 1
	itr += 2
    return prime


if __name__ == '__main__':
    print findNthPrime(10001)
