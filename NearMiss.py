#NEAR MISS problem from LCCC 2016
import math
rocket_pos = map(float,raw_input().strip().split(' '))
rocket_vel = map(float,raw_input().strip().split(' '))
comet_pos = map(float,raw_input().strip().split(' '))
comet_vel = map(float,raw_input().strip().split(' '))

t_num = (rocket_vel[0]-comet_vel[0])*(comet_pos[0]-rocket_pos[0])+(rocket_vel[1]-comet_vel[1])*(comet_pos[1]-rocket_pos[1])+(rocket_vel[2]-comet_vel[2])*(comet_pos[2]-rocket_pos[2])
t_denom = (rocket_vel[0]-comet_vel[0])**2 + (rocket_vel[1]-comet_vel[1])**2 + (rocket_vel[2]-comet_vel[2])**2
if t_num <0 or t_denom==0:
    print math.sqrt((rocket_pos[0] - comet_pos[0])**2+(rocket_pos[1] - comet_pos[1])**2 + (rocket_pos[2] - comet_pos[2])**2)
else:
    t = float(t_num)/t_denom

    x_diff = (rocket_pos[0]+t*(rocket_vel[0])) - (comet_pos[0]+t*(comet_vel[0]))
    y_diff = (rocket_pos[1]+t*(rocket_vel[1])) - (comet_pos[1]+t*(comet_vel[1]))
    z_diff = (rocket_pos[2]+t*(rocket_vel[2])) - (comet_pos[2]+t*(comet_vel[2]))

    print math.sqrt(x_diff**2+y_diff**2+z_diff**2)