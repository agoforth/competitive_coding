#Power of N problem from LCCC 2016
import math 
k = float(raw_input())
n = float(raw_input())
if n==1 and k==1:
    print "YES"
elif n==1 and k != 1:
    print "NO"
else:
    k = math.log(k,n)
    if int(k) == k:
        print "YES"
    else:
        print "NO"