#EGG DROP PROBLEM

"""
If we have 2 eggs to test with and n floors,
how can we find the first floor that the egg breaks on?

PSEUDOCODE:
i=1
Check floor n-10*i:					(90)
	If doesn't break, i+=1
	Else check floor n-10*i+10-i:		(91)
		If doesn't break, i+=1
		Else return n-10*i+10-i
"""

#DOESN'T QUITE WORK

import numpy as np

#n floors
#k eggs
def egg_drop(n,k):
	"""
	Inputs: 	n = number of floors
				k = number of eggs
	Returns: 	number of floors tested in worst case scenario
	"""
	current = [[0 for x in range(n+1)] for x in range(k+1)]
	for i in range(1,k+1):
		current[i][1]=1
		current[i][0]=0
	for j in range(1,n+1):
		current[1][j]=j
	for i in range(2,k+1):
		for j in range(2,n+1):
			#print current[i][j]
			current[i][j] = n**2 # just a big number
			#print current[i][j]
			for x in range(1,j+1):
				a = 1+max(current[i-1][x-1], current[i][j-x])
				if a < current[i][j]:
					current[i][j]=a
	return current[k][n]


print egg_drop(100,2)


#trying to check every 10 floors
"""def find_floor(n,fail):
	#print fail
	i = 1
	j = 1

	num_floors_tested = 0
	failed = False
	while failed == False:
		if 10*i >= fail:
			num_floors_tested += 1
			i += 1
		else:
			if 10*i-(10-j) >= fail:
				num_floors_tested += 1
				j += 1
			else:
				failed = True
	return num_floors_tested


floors = []
for i in xrange(1000):
	fail = np.random.randint(1,1000)
	floors.append(find_floor(1000,fail))
print np.mean(floors)"""